$(document).ready(function () {
    jQuery.validator.addMethod("validate_email", function (value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value))
        { return true; } 
        else {return false; }
    }, "Please enter a valid email.");

    jQuery.validator.addMethod("lettersonlys", function (value, element) {
        return this.optional(element) || /^[ A-Za-z]*$/.test(value);
    }, "PLease enter valid name");

    jQuery.validator.addMethod('positiveNumber', function (value,element) {
        return this.optional(element) || /^[0-9]*$/.test(value);
    }, 'Please enter valid price.');


    $("#frmUpdate").validate({
        rules: {
            name: {lettersonlys: true},
            email: {validate_email: true},
            contact: {positiveNumber: true}
        },
        messages: {
            name: {lettersNoonlys: "Please enter valid Name"},
            email: {validate_email: "Please enter valid email"},
            contact: {positiveNumber: "Please enter valid Contact No"}
        },
        wrapper: 'lable',
        submitHandler: function (form) {
            var userId = $('#userId').val();
            if(userId==""){
                msg = "Add";
            }else{
                msg = "Update";
            }
            swal({
                title:"Are you sure! Do you want to "+msg+" data?",
                type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
            },function(){

                var formData = $('#frmUpdate').serialize();
                $.ajax({
                    url: 'profile',
                    type: "POST",
                    data: formData,
                    success: function (response) {
                        if (response.status == 'error')
                        {
                            swal("", response.msg, "error");
                            return false;
                        } else
                        {
                            swal("", response.msg, "success");
                            setTimeout(function(){ 
                            window.location.href = response.red_url
                            }, 3000);
                        }
                    }
                });
           
         } );
        }
        
    });

    $("#frmChngPass").validate({
        wrapper: 'lable',
        rules: {
            old_pass: {
                required: true,
                remote: {
                    url: 'check_password',
                    type: "post",
                    data: {
                        userId: function () {
                            return $("#userId").val();
                        },
                        csrfmiddlewaretoken:function(){
                            csrfmiddlewaretoken = jQuery("[name=csrfmiddlewaretoken]").val();
                            return csrfmiddlewaretoken
                        }
                    }
                }
            },
            new_pass: { required: true},
            confirm_pass: {
                required: true,
                equalTo: "#new_pass"
            }
        },
        messages: {
            old_pass: {required: "Old Password is required",remote:"Invalid Old Password"},
            new_pass: {required: "New Password is required"},
            confirm_pass: {required: "Confirm Password is required", equalTo: "Confirm Password does not match with New Password"}
           
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'change_password',
                type: "post",
                data: $("#frmChngPass").serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status == 'error')
                    {
                        swal("", response.msg, "error");
                        return false;
                    } else
                    {
                        swal("", response.msg, "success");
                        window.location.href = response.red_url;
                    }
                }
            });
        }
    });
        
    
});
