$(document).ready(function () {
    $("#frmLogin").validate({
        rules: {
            username: { required: true},
            password: {required: true}

        },
        messages: {
            username: {required: "Username is required"},
            password: {required: "Password is required"}
        },
        wrapper: 'lable',
        submitHandler: function (form) {
            $.ajax({
                url: '',
                type: "POST",
                data: $("#frmLogin").serialize(),
                //dataType: "json",
                success: function (response) {
                    if (response.status == 'error')
                    {
                        swal("", response.msg, "error");
                        return false;
                    } else
                    {
                        window.location.href = response.red_url;
                        /*swal("", response.msg, "success");
                        setTimeout(function(){ 
                           window.location.href = response.red_url;
                        }, 1000);*/
                    }
                }
            });
        }
    });
	
	 $("#frmUpPassword").validate({
        wrapper: 'lable',
        submitHandler: function (form) {
			
            load_image();
            $.ajax({
                url: 'home/adminLogin',
                type: "post",
                data: $("#frmUpPassword").serialize(),
                dataType: "json",
                success: function (response) {
                    //console.log(response);
                    if (response.status == 'error')
                    {
                        unload_image();
                        show_error(response.msg);
                        return false;
                    } else
                    {
                        unload_image();
                        window.location.href = base_url+'dashboard';
                    }
                }
            });
        }
    });
    
});
