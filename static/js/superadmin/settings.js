$(document).ready(function () {

    (function($) {
        "use strict"
    
    
        //todo list
        $(".tdl-new").on('keypress', function(e) {
    
            var code = (e.keyCode ? e.keyCode : e.which);
    
            if (code == 13) {
    
                var v = $(this).val();
    
                var s = v.replace(/ +?/g, '');
    
                if (s == "") {
    
                    return false;
    
                } else {
    
                    $(".tdl-content ul").append("<li><label><input type='checkbox'><i></i><span>" + v + "</span><a href='#' class='ti-trash'></a></label></li>");
    
                    $(this).val("");
    
                }
    
            }
    
        });
    
    
        $(".tdl-content a").on("click", function() {
    
            var _li = $(this).parent().parent("li");
    
            _li.addClass("remove").stop().delay(100).slideUp("fast", function() {
    
                _li.remove();
    
            });
    
            return false;
    
        });
    
    
    
        // for dynamically created a tags
    
        $(".tdl-content").on('click', "a", function() {
    
            var _li = $(this).parent().parent("li");
    
            _li.addClass("remove").stop().delay(100).slideUp("fast", function() {
    
                _li.remove();
    
            });
    
            return false;
    
        });
    
    })(jQuery);



    //Add Floor
    $("#addBrandBtn").on('keypress', function(e) {
    
        var code = (e.keyCode ? e.keyCode : e.which);

        if (code == 13) {

            var floor = $(this).val();

            var brand_name = floor.replace(/ +?/g, '');

            if (brand_name == "") {

                return false;

            } else {

                var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
                swal({
                    title:"Are you sure! Do you want to add this Brand?",
                    type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
                },function(){
                    
                    $.ajax({
                        url: 'add_brand',
                        type: "POST",
                        data: {'brand_name':brand_name, 'csrfmiddlewaretoken':csrftoken},
                        success: function (response) {
                            if (response.status == 'error')
                            {
                                swal("", response.msg, "error");
                                return false;
                            } else
                            {
                                swal("", response.msg, "success");
                                var brand_id = response.brand_id
                                $(".tdl-floor ul").append("<li><label class='floor_li"+brand_id+"'><span>" + brand_name + "</span><a href='#' onclick='deleteBrand("+brand_id+")'  class='ti-trash'></a></label></li>");
                                $('#addBrandBtn').val(" ");
                            }
                        }
                    });
                });

            }

        }

    });

  
    
});


function deleteBrand(brandId=null) {
    
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to delete this Brand?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'delete_brand',
            type: "POST",
            data: {'brandId':brandId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    $('.floor_li'+brandId).remove();
            
                    return false;
                }
            }
        });
    });
}
