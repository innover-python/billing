
(function($) {
    "use strict"

    let ctx = document.getElementById("chart_widget_2");
    ctx.height = 280;
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["2010", "2011", "2012", "2013", "2014", "2015", "2016"],
            type: 'line',
            defaultFontFamily: 'Montserrat',
            datasets: [{
                data: [0, 15, 57, 12, 85, 10, 50],
                label: "iPhone X",
                backgroundColor: '#847DFA',
                borderColor: '#847DFA',
                borderWidth: 0.5,
                pointStyle: 'circle',
                pointRadius: 5,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#847DFA',
            }, {
                label: "Pixel 2",
                data: [0, 30, 5, 53, 15, 55, 0],
                backgroundColor: '#F196B0',
                borderColor: '#F196B0',
                borderWidth: 0.5,
                pointStyle: 'circle',
                pointRadius: 5,
                pointBorderColor: 'transparent',
                pointBackgroundColor: '#F196B0',
            }]
        },
        options: {
            responsive: !0,
            maintainAspectRatio: false,
            tooltips: {
                mode: 'index',
                titleFontSize: 12,
                titleFontColor: '#000',
                bodyFontColor: '#000',
                backgroundColor: '#fff',
                titleFontFamily: 'Montserrat',
                bodyFontFamily: 'Montserrat',
                cornerRadius: 3,
                intersect: false,
            },
            legend: {
                display: false,
                position: 'top',
                labels: {
                    usePointStyle: true,
                    fontFamily: 'Montserrat',
                },


            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            },
            title: {
                display: false,
            }
        }
    });


})(jQuery);

function updateDelivery(orderId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to update this Order as Delivered?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'update_order_status',
            type: "POST",
            data: {'orderId':orderId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.reload();
                    }, 1000);
                }
            }
        });
    });
}

function updatePayment(orderId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to update this Order Payment as Paid?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'update_payment_status',
            type: "POST",
            data: {'orderId':orderId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.reload();
                    }, 1000);
                }
            }
        });
    });
}