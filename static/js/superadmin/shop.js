$(document).ready(function () {
   
    jQuery.validator.addMethod("validate_email", function (value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value))
        { return true; } 
        else {return false; }
    }, "Please enter a valid email.");

    jQuery.validator.addMethod("lettersonlys", function (value, element) {
        return this.optional(element) || /^[ A-Za-z]*$/.test(value);
    }, "PLease enter valid name");

    jQuery.validator.addMethod('positiveNumber', function (value,element) {
        return this.optional(element) || /^[0-9]*$/.test(value);
    }, 'Please enter valid price.');

    jQuery('#followup_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $("#frmShop").validate({
        rules: {
            name: {lettersonlys: true},
            contact: {positiveNumber: true},
            /*email: {validate_email: true}*/
        },
        messages: {
            name: {lettersNoonlys: "Please enter valid Name"},
            contact: {positiveNumber: "Please enter valid Mobile No"},
            /*email: {validate_email: "Please enter valid Email ID"}*/
        },
        wrapper: 'lable',
        submitHandler: function (form) {
            var shop_id = $('#shopId').val();
            if(shop_id==""){
                msg = "Add";
            }else{
                msg = "Update";
            }
            swal({
                title:"Are you sure! Do you want to "+msg+" Shop?",
                type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
            },function(){
                var formData = new FormData($('#frmShop')[0]);
                $.ajax({
                    url: add_shop_url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 'error')
                        {
                            swal("", response.msg, "error");
                            return false;
                        } else
                        {
                            swal("", response.msg, "success");
                            setTimeout(function(){ 
                            window.location.href = shop_url;
                            }, 1000);
                        }
                    }
                });
           
         } );
        }
        
    });

    

});



function deleteShop(shopId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to delete this Shop?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'delete_shop',
            type: "POST",
            data: {'shopId':shopId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.href = response.red_url;
                    }, 1000);
                }
            }
        });
    });
}

