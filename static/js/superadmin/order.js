$(document).ready(function () {
   
    jQuery.validator.addMethod("validate_email", function (value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value))
        { return true; } 
        else {return false; }
    }, "Please enter a valid email.");

    jQuery.validator.addMethod("lettersonlys", function (value, element) {
        return this.optional(element) || /^[ A-Za-z]*$/.test(value);
    }, "PLease enter valid name");

    jQuery.validator.addMethod('positiveNumber', function (value,element) {
        return this.optional(element) || /^[0-9]*$/.test(value);
    }, 'Please enter valid price.');

    jQuery('#followup_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $("#frmOrder").validate({
        wrapper: 'lable',
        submitHandler: function (form) {
            var is_true = 0;
            $(".prod_id").each(function () {
                if($(this).val() > 0){
                    is_true=1;
                }
            });
            if (is_true == 0) {
                swal("", "Please select atleast one one product !", "error");
                return false;
            } else {
                var enq_id = $('#orderId').val();
                if(enq_id==""){
                    msg = "Create";
                }else{
                    msg = "Update";
                }
                swal({
                    title:"Are you sure! Do you want to "+msg+" Order?",
                    type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
                },function(){
                    var formData = new FormData($('#frmOrder')[0]);
                    $.ajax({
                        url: add_order_url,
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (response) {
                            if (response.status == 'error')
                            {
                                swal("", response.msg, "error");
                                return false;
                            } else
                            {
                                swal("", response.msg, "success");
                                setTimeout(function(){ 
                                window.location.href = order_url;
                                }, 1000);
                            }
                        }
                    });
            
                });
            }
            
        }
        
    });

    $('#prod_srch').change(function () {
        var fval = $(this).val();
        $('#product_id').val(fval);
    });

    var prd_arr = [];
    $("#frmProduct").validate({
        rules: {
            prod_qty: {positiveNumber: true},
        },
        messages: {
            prod_qty: {positiveNumber: "Please enter valid Quantity"}
        },
        wrapper: 'lable',
        submitHandler: function (form) {
            var prod_qty = $("#prod_qty").val();
            var product_id = $("#product_id").val();
            
            prd_arr.push(product_id);
            $('#remov_row').remove();

            var is_true = 1;
            var i=0;
            $(".prod_id").each(function () {
                if ($(this).val() == product_id) {
                    swal("","Product already added in list!", "error");
                    is_true = 0;
                }
                i++;
            });

            if (is_true == 0) {
                return false;
            }
            if (product_id != '') {
                //load_image();
                $.ajax({
                    url: get_product_details,
                    type: "post",
                    dataType: "json",
                    data: $("#frmProduct").serialize(),
                    success: function (response) {
                        //unload_image();
                        if (response.status == 'error')
                        {
                            swal("", response.msg, "error");
                            return false;
                        } else
                        {
                            var srCnt = parseInt($("#srno").val());
                            var srl = parseInt(srCnt) + parseInt(1);
                            $("#srno").val(srl);
                            var srNo = parseInt($("#srno").val());

                            var tot_price = (prod_qty*response.data.mrp);
                            $('#prod_tbl').append('<tr id="tr_' + srNo + '"><td id="sro_' + srNo + '">' + srNo + '</td><td><input type="hidden" class="prod_id" name="prod_id[]" value="' + product_id + '" /><input type="hidden" name="prod_name[]" value="' + response.data.product_name + '" /><input type="hidden" class="prod_price_' + product_id + '" name="price[]" value="' + response.data.mrp + '" />' + response.data.product_name + '</td><td>' + response.data.mrp + '</td><td><input class="form-control cal_qty" product_id="'+product_id+'" type="text" name="qty[]" value="' + prod_qty + '" /></td><td><input class="form-control cal_price price_' + product_id + '"  type="text" name="tot_price[]" value="' + tot_price + '" readonly/></td><td><a href="javascript:void(0);" data-toggle="tooltip" title="Delete" onclick="del_prod_menu(this)" d-id=' + product_id + ' id=dela_' + srNo + '  class=' + srNo + '><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o fa-1x"></i></button></a></td></tr>');
                            $('#frmProduct')[0].reset();

                            var total_price=0;
                            $(".cal_price").each(function () {
                                if ($(this).val() != "") {
                                    total_price = (Number(total_price)+Number($(this).val()));
                                }
                            });
                            $('#grand_total').val(total_price);
                            $('#grand_total_txt').html(total_price);
                            swal("", "Product added in list successfully", "success");
                        }
                    }
                });
            } else {
                swal("Please select product!", response.msg, "error");
                return false;
            }
        }
    });


    $(document).on('keyup', '.cal_qty', function (event)
    {
        var qty = $(this).val();
        var prod_id = $(this).attr('product_id');
        var prod_price = $('.prod_price_'+prod_id).val();
        var tot_price = (Number(prod_price)*Number(qty));
        $('.price_'+prod_id).val(tot_price);
        
        var total_price=0;
        $(".cal_price").each(function () {
            if ($(this).val() != "") {
                total_price = (Number(total_price)+Number($(this).val()));
            }
        });
        $('#grand_total').val(total_price);
        $('#grand_total_txt').html(total_price);
       
    });

});


function del_prod_menu(obj) {
    var srno = $(obj).attr('class');
    var isConf = confirm('Are you sure! do you want to remove this product?');
    if (isConf == true) {
        //load_image();
        var rec_cnt = $("#srno").val();

        var nxtsrno = parseInt(srno) + parseInt(1);

        for (var i = nxtsrno; i <= rec_cnt; i++) {
            var srTxt = (parseInt(i) - parseInt(1));
            $("#sro_" + i).text(srTxt);
            $("#sro_" + i).attr("id", "sro_" + srTxt);
            $("#dela_" + i).attr('class', srTxt);
            $("#dela_" + i).attr("id", "dela_" + srTxt);
            $("#tr_" + i).attr("id", "tr_" + srTxt);
        }

        $("#tr_" + srno).remove();
        var cntSrn = (rec_cnt - parseInt(1));
        $("#srno").val(cntSrn);

        if ($('table tbody tr').length == 0)
        {
            $('#prod_tbl').append('<tr id="remov_row"><td colspan="100%" style="text-align:center"><strong>No data</strong></td></tr>');
        }
        $('.tooltip').remove();

        var total_price=0;
        $(".cal_price").each(function () {
            if ($(this).val() != "") {
                total_price = (Number(total_price)+Number($(this).val()));
            }
        });
        $('#grand_total').val(total_price);
        $('#grand_total_txt').html(total_price);
    }
   
}

function deleteOrder(orderId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to delete this Order?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'delete_order',
            type: "POST",
            data: {'orderId':orderId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.href = response.red_url;
                    }, 1000);
                }
            }
        });
    });
}

function updateDelivery(orderId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to update this Order as Delivered?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'update_order_status',
            type: "POST",
            data: {'orderId':orderId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.reload();
                    }, 1000);
                }
            }
        });
    });
}

function updatePayment(orderId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to update this Order Payment as Paid?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'update_payment_status',
            type: "POST",
            data: {'orderId':orderId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.reload();
                    }, 1000);
                }
            }
        });
    });
}
