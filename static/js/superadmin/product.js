$(document).ready(function () {
   
    jQuery.validator.addMethod("validate_email", function (value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value))
        { return true; } 
        else {return false; }
    }, "Please enter a valid email.");

    jQuery.validator.addMethod("lettersonlys", function (value, element) {
        return this.optional(element) || /^[ A-Za-z]*$/.test(value);
    }, "PLease enter valid name");

    jQuery.validator.addMethod('positiveNumber', function (value,element) {
        return this.optional(element) || /^[0-9]*$/.test(value);
    }, 'Please enter valid price.');

    jQuery('#followup_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $("#frmProduct").validate({
        rules: {
            weight: {positiveNumber: true},
            order_level: {positiveNumber: true},
            alert_level: {positiveNumber: true},
        },
        messages: {
            weight: {positiveNumber: "Please enter valid Weight / Quantity"},
            order_level: {positiveNumber: "Please enter valid Order Level"},
            alert_level: {positiveNumber: "Please enter valid Alert Level"},
        },
        wrapper: 'lable',
        submitHandler: function (form) {
            var enq_id = $('#prodId').val();
            if(enq_id==""){
                msg = "Add";
            }else{
                msg = "Update";
            }
            swal({
                title:"Are you sure! Do you want to "+msg+" Product?",
                type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
            },function(){
                var formData = new FormData($('#frmProduct')[0]);
                $.ajax({
                    url: add_product_url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 'error')
                        {
                            swal("", response.msg, "error");
                            return false;
                        } else
                        {
                            swal("", response.msg, "success");
                            setTimeout(function(){ 
                            window.location.href = product_url;
                            }, 1000);
                        }
                    }
                });
           
         } );
        }
        
    });

    

});



function deleteProduct(prodId=null) {
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    swal({
        title:"Are you sure! Do you want to delete this Product?",
        type:"info",showCancelButton:!0,closeOnConfirm:!1,showLoaderOnConfirm:!0
    },function(){
        
        $.ajax({
            url: 'delete_product',
            type: "POST",
            data: {'prodId':prodId, 'csrfmiddlewaretoken':csrftoken},
            success: function (response) {
                if (response.status == 'error')
                {
                    swal("", response.msg, "error");
                    return false;
                } else
                {
                    swal("", response.msg, "success");
                    setTimeout(function(){ 
                       window.location.href = response.red_url;
                    }, 1000);
                }
            }
        });
    });
}

