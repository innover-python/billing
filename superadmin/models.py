from django.db import models

# Create your models here.
class Employees(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    password = models.CharField(max_length=100)
    group_id = models.IntegerField()
    contact = models.IntegerField()
    address = models.TextField()
    access_token = models.CharField(max_length=100)
    is_active = models.IntegerField(default=1)
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    modified_date = models.DateTimeField(auto_now=False, auto_now_add=False)

class Shop(models.Model):
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    owner = models.CharField(max_length=250)
    contact = models.CharField(max_length=15)
    address = models.TextField()
    photo = models.ImageField(upload_to='pics')
    is_active = models.IntegerField(default=1)
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False) 
    modified_date = models.DateTimeField(auto_now=False,auto_now_add=False)   

class Brand(models.Model):
    name = models.CharField(max_length=100)
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False)  
    
class Product(models.Model):
    product_name = models.CharField(max_length=250)
    code = models.CharField(max_length=50)
    brand = models.ForeignKey('superadmin.Brand', on_delete=models.CASCADE)
    weight = models.CharField(max_length=250)
    unit = models.CharField(max_length=30)
    price = models.FloatField()
    mrp = models.FloatField()
    order_level = models.IntegerField()
    alert_level = models.IntegerField()
    description = models.TextField()
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False)  
    modified_date = models.DateTimeField(auto_now=False,auto_now_add=False)            


class Order(models.Model):
    order_no = models.CharField(max_length=10)
    shop = models.ForeignKey('superadmin.Shop', on_delete=models.CASCADE)
    total_amount = models.FloatField()
    discount = models.FloatField()
    grand_total = models.FloatField()
    status = models.IntegerField(default=0)
    payment = models.IntegerField(default=0)
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False) 
    modified_date = models.DateTimeField(auto_now=False, auto_now_add=False) 


class Order_details(models.Model):
    order_id = models.ForeignKey('superadmin.Order', on_delete=models.CASCADE)
    shop_id = models.ForeignKey('superadmin.Shop', on_delete=models.CASCADE)
    prod_id = models.ForeignKey('superadmin.Product', on_delete=models.CASCADE)
    prod_name = models.CharField(max_length=30)
    qty = models.IntegerField()
    price = models.FloatField()
    tot_price = models.FloatField()
    is_delete = models.IntegerField(default=0)
    added_date = models.DateTimeField(auto_now=False, auto_now_add=False) 




  