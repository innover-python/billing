from django.shortcuts import render, redirect

from ..models import Brand

#for messages
from django.contrib import messages

from django.http import JsonResponse

from django.utils import timezone

#for Media
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from datetime import date


# Create your views here.
def settings(request):
    brands = Brand.objects.filter(is_delete=0).order_by('name')
    return render(request, 'superadmin/settings.html',{ 'brands':brands })  

def add_brand(request):
    if request.method == 'POST':
        name = request.POST['brand_name']
        added_date = timezone.now()

        brand_data = Brand(name=name, added_date=added_date)
        brand_data.save()
        brand_id = brand_data.id
        msg = "Brand has been added successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'brand_id':brand_id,  'red_url': 'settings'}
        )

    else:
        return render(request, 'superadmin/settings.html') 


def delete_brand(request):  
    if request.method == 'POST':
        brandId = request.POST['brandId']
        Brand.objects.filter(id=brandId).update(is_delete='1')
        return JsonResponse(
           {'status':'success', 'msg':'Brand has been deleted successfully!', 'red_url': 'settings'}
        )

