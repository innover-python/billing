from django.shortcuts import render, redirect

#from receptionist.models import Visitor,Followup
from ..models import Employees, Product, Shop, Order

#for messages
from django.contrib import messages

from django.http import JsonResponse

from django.utils import timezone

from datetime import date

from django.db.models import Sum


# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        try:
            user = Employees.objects.get(email=username, password=password, group_id=1, is_active=1, is_delete=0)
        except Employees.DoesNotExist:
            user = None

        if user is not None:
            request.session["loginId"] = user.id
            request.session["loginName"] = user.name
            request.session["accessToken"] = user.access_token
            return JsonResponse(
                {'status':'success', 'msg':'User logged in successfully!', 'red_url': 'dashboard'}
                )
        else:
            return JsonResponse(
                {'status':'error', 'msg':'Username or Password does not match!'}
                )
    else:
        return render(request, 'superadmin/login.html')

def logout(request):
    del request.session['loginId']
    del request.session['loginName']
    del request.session['accessToken']
    return redirect('/')   

def dashboard(request):
    #Shops
    shops = Shop.objects.filter(is_delete=0, is_active=1).count()

    #Products
    products = Product.objects.filter(is_delete=0).count()
       
    #Today's Order 
    todays_order = Order.objects.filter(is_delete=0, added_date__startswith=date.today()).order_by('-id')
    todays_order_cnt = todays_order.count()

    #Total Sale
    saleData = Order.objects.filter(is_delete = 0).aggregate(Sum('grand_total'))
    totSale = saleData['grand_total__sum']
    result = {
        'page':'dashboard',
        'shops':shops,
        'products':products, 
        'todays_order_cnt':todays_order_cnt,
        'todays_order':todays_order,
        'tot_sale':totSale
    }

    return render(request, 'superadmin/dashboard.html', result)  

def profile(request):
    if request.method == 'POST':
        userId = request.POST['userId']
        name = request.POST['name']
        email = request.POST['email']
        contact = request.POST['contact']
        address = request.POST['address']

        Employees.objects.filter(id=userId).update(name=name, contact=contact, email=email, address=address)
        msg  = "Profile has been updated successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'red_url': 'profile'}
        )
    user_id = request.session['loginId']
    userData = Employees.objects.get(id=user_id)
    return render(request, 'superadmin/profile.html', {'action':'Edit', 'userData':userData})  


def check_password(request):
    if request.method == 'POST' :
        userId = request.POST['userId']
        old_pass = request.POST['old_pass']
        try:
            data = Employees.objects.get(id=userId,password=old_pass)
        except Employees.DoesNotExist:
            data = None
    
        if data is not None:
            return JsonResponse(
                {'status':'success', 'msg':'true'}
                )
        else:
            return JsonResponse(
                {'status':'error', 'msg':'false'}
                )

def change_password(request):
    if request.method == 'POST':
        userId = request.POST['userId']
        new_pass = request.POST['new_pass']

        Employees.objects.filter(id=userId).update(password=new_pass)
        msg  = "Password has been updated successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'red_url': 'profile'}
        )