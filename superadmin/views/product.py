from django.shortcuts import render, redirect

from ..models import Brand, Product

#for messages
from django.contrib import messages

from django.http import JsonResponse

from django.utils import timezone

#for Media
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from datetime import date


# Create your views here.

def product(request):
    record = Product.objects.filter(is_delete=0).order_by('-id').select_related()
    return render(request, 'superadmin/product/manage.html', {'record':record})

def add_product(request,prodId=0):

    brands = Brand.objects.filter(is_delete=0).order_by('name')
    if request.method == 'POST':
        prodId = request.POST['prodId']
        product_name = request.POST['product_name'].title()
        code = request.POST['code']
        brand = request.POST['brand']
        weight = request.POST['weight']
        unit = request.POST['unit']
        price = request.POST['price']
        mrp = request.POST['mrp']
        order_level = request.POST['order_level']
        alert_level = request.POST['alert_level']
        description = request.POST['description']

        added_date = timezone.now()
        if prodId == "":
            flat_data = Product(product_name=product_name, code=code, brand_id=brand, weight=weight, unit=unit, price=price, mrp=mrp, order_level=order_level, alert_level=alert_level, description=description, added_date=added_date)
            flat_data.save()
            msg = "Product has been added successfully!"
        else:
            Product.objects.filter(id=prodId).update(product_name=product_name, code=code, brand_id=brand, weight=weight, unit=unit, price=price, mrp=mrp, order_level=order_level, alert_level=alert_level, description=description, modified_date=added_date)
            msg  = "Product has been updated successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'red_url': 'product'}
        )
    elif prodId != 0:
        record = Product.objects.get(id=prodId)
        return render(request, 'superadmin/product/add.html', {'action':'Edit', 'record':record, 'brands':brands})

    else:  
        return render(request, 'superadmin/product/add.html', {'action':'Add', 'brands':brands})    

def delete_product(request):  
    if request.method == 'POST':
        prodId = request.POST['prodId']
        Product.objects.filter(id=prodId).update(is_delete='1')
        return JsonResponse(
           {'status':'success', 'msg':'Product has been deleted successfully!', 'red_url': 'product'}
        )

def get_product_details(request):  
    if request.method == 'POST':
        prodId = request.POST['product_id']
       
        prodData = Product.objects.values('product_name','mrp').get(id=prodId)
        return JsonResponse(
           {'status':'success', 'data': prodData}
        )    

