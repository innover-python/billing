from django.shortcuts import render, redirect

from ..models import Shop

#for messages
from django.contrib import messages

from django.http import JsonResponse

from django.utils import timezone

#for Media
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from datetime import date


# Create your views here.
def shop(request):
    record = Shop.objects.filter(is_delete=0).order_by('-id')
    return render(request, 'superadmin/shop/manage.html', {'record':record})

def add_shop(request,shopId=0):

    if request.method == 'POST':
        shopId = request.POST['shopId']
        name = request.POST['name'].title()
        owner = request.POST['owner'].title()
        contact = request.POST['contact']
        email = request.POST['email']
        address = request.POST['address']
        #Upload Photo
        #photo = request.FILES['photo']
      

        added_date = timezone.now()
        if shopId == "":
            member_data = Shop(name=name, owner=owner, contact=contact, email=email, address=address, added_date=added_date)
            member_data.save()
            msg = "Shop has been added successfully!"
        else:
            Shop.objects.filter(id=shopId).update(name=name, owner=owner, email=email, contact=contact, modified_date=added_date)
            msg  = "Shop has been updated successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'red_url': 'shop'}
        )
    elif shopId != 0:
        record = Shop.objects.get(id=shopId)
        return render(request, 'superadmin/shop/add.html', {'action':'Edit', 'record':record})

    else:  
        return render(request, 'superadmin/shop/add.html', {'action':'Add'})    

def delete_shop(request):  
    if request.method == 'POST':
        shopId = request.POST['shopId']
        Shop.objects.filter(id=shopId).update(is_delete='1')
        return JsonResponse(
           {'status':'success', 'msg':'Shop has been deleted successfully!', 'red_url': 'shop'}
        )
