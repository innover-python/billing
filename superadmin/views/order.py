from django.shortcuts import render, redirect

from ..models import Product, Shop, Order, Order_details, Employees

#for messages
from django.contrib import messages

from django.http import JsonResponse

from django.utils import timezone

#for Media
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from datetime import date


# Create your views here.

def order(request):
    record = Order.objects.filter(is_delete=0, status=0).order_by('-id').select_related()
    return render(request, 'superadmin/order/manage.html', {'record':record})

def add_order(request,orderId=0):
    record = order_details = []
    products = Product.objects.filter(is_delete=0).order_by('product_name')
    shops = Shop.objects.filter(is_delete=0).order_by('name')
    if request.method == 'POST':
        orderId = request.POST['orderId']
        shop = request.POST['shop']
        total_amount = request.POST['grand_total']
        discount = "0"
        grand_total = request.POST['grand_total']

        prodId = request.POST.getlist('prod_id[]')
        prodName = request.POST.getlist('prod_name[]')
        prodPrice = request.POST.getlist('price[]')
        prodQty = request.POST.getlist('qty[]')
        prodTotPrice = request.POST.getlist('tot_price[]')

        added_date = timezone.now()
        if orderId == "":
            order_data = Order(shop_id=shop, total_amount=total_amount, discount=discount, grand_total=grand_total, added_date=added_date)
            order_data.save()
            order_id = order_data.id

            #Update Order No
            order_no = (1000+int(order_id))
            Order.objects.filter(id=order_id).update(order_no=order_no)

            #Insert Order details
            i=0
            for prod in prodId:
                order_details = Order_details(order_id_id=order_id, shop_id_id=shop, prod_id_id=prod, prod_name=prodName[i], qty=prodQty[i], price=prodPrice[i], tot_price=prodTotPrice[i], added_date=added_date )
                order_details.save()
                i = i+1
            msg = "Order has been created successfully!"

        else:            
            Order.objects.filter(id=orderId).update(shop_id=shop, total_amount=total_amount, discount=discount, grand_total=grand_total, modified_date=added_date)
            
            #Delete Previous Order Details
            Order_details.objects.filter(order_id_id=orderId).delete()

            #Insert Order details
            i=0
            for prod in prodId:
                order_details = Order_details(order_id_id=orderId, shop_id_id=shop, prod_id_id=prod, prod_name=prodName[i], qty=prodQty[i], price=prodPrice[i], tot_price=prodTotPrice[i], added_date=added_date )
                order_details.save()
                i = i+1
            
            msg  = "Order has been updated successfully!"
        
        return JsonResponse(
           {'status':'success', 'msg':msg, 'red_url': 'order'}
        )
    elif orderId != 0:
        record = Order.objects.get(id=orderId)
        order_details = Order_details.objects.filter(order_id=orderId).select_related()
        return render(request, 'superadmin/order/add.html', {'action':'Edit', 'record':record, 'order_details':order_details, 'shops':shops, 'products':products})

    else:  
        return render(request, 'superadmin/order/add.html', {'action':'Add', 'record':record, 'order_details':order_details, 'shops':shops, 'products':products})    


def view_order(request,orderId,fromId=0):
    record = order_details = []
    user_id = request.session['loginId']
    userData = Employees.objects.get(id=user_id)
    record = Order.objects.filter(id=orderId).select_related()
    order_details = Order_details.objects.filter(order_id=orderId).select_related()
    return render(request, 'superadmin/order/view.html', {'fromId':fromId, 'record':record, 'order_details':order_details, 'userData':userData})


def delivered_order(request):
    record = Order.objects.filter(is_delete=0, status=1).order_by('-id').select_related()
    return render(request, 'superadmin/order/delivered_order.html', {'record':record})

def delete_order(request):  
    if request.method == 'POST':
        orderId = request.POST['orderId']
        Order.objects.filter(id=orderId).update(is_delete='1')
        return JsonResponse(
           {'status':'success', 'msg':'Order has been deleted successfully!', 'red_url': 'order'}
        )

def update_order_status(request):  
    if request.method == 'POST':
        orderId = request.POST['orderId']
        Order.objects.filter(id=orderId).update(status='1')
        return JsonResponse(
           {'status':'success', 'msg':'Order has been delivered successfully!', 'red_url': 'order'}
        )  

def update_payment_status(request):  
    if request.method == 'POST':
        orderId = request.POST['orderId']
        Order.objects.filter(id=orderId).update(payment='1')
        return JsonResponse(
           {'status':'success', 'msg':'Order payment has been paid successfully!', 'red_url': 'order'}
        )                

