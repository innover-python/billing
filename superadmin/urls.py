from django.urls import path

from .views import views
from .views import shop
from .views import product
from .views import order
from .views import settings

urlpatterns = [
    path('', views.login, name='index'),
    path('logout', views.logout, name='logout'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('profile', views.profile, name='profile'),
    path('check_password',views.check_password,name='check_password'),
    path('change_password',views.change_password,name='change_password'),

    #Shop View
    path('shop', shop.shop, name='shop'),
    path('add_shop', shop.add_shop, name='add_shop'),
    path('add_shop/<int:shopId>/', shop.add_shop, name='add_shop'),
    path('delete_shop', shop.delete_shop, name='delete_shop'),

    #Product View
    path('product', product.product, name='product'),
    path('add_product', product.add_product, name='add_product'),
    path('add_product/<int:prodId>/', product.add_product, name='add_product'),
    path('delete_product', product.delete_product, name='delete_product'),
    path('get_product_details', product.get_product_details, name='get_product_details'),

    #Setting View
    path('settings', settings.settings, name='settings'),
    path('add_brand', settings.add_brand, name='add_brand'),
    path('delete_brand', settings.delete_brand, name='delete_brand'),

    #Order View
    path('order', order.order, name='order'),
    path('add_order', order.add_order, name='add_order'),
    path('add_order/<int:orderId>/', order.add_order, name='add_order'),
    path('view_order/<int:orderId>/<int:fromId>/', order.view_order, name='view_order'),
    path('delivered_order', order.delivered_order, name='delivered_order'),
    path('delete_order', order.delete_order, name='delete_order'),
    path('update_order_status', order.update_order_status, name='update_order_status'),
    path('update_payment_status', order.update_payment_status, name='update_payment_status'),
   
]